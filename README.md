# bi-amaro

This repository contains the answers to the AMARO Data Engineer Case Study for 
every question that demanded code to be written.

The Deliveries 5 and 6 are in Amazon Redshift Language and can be run directly
into a Redshift cluster provided that it contains the orders.csv data.

The Part II is the API written in Python using the Flask module. It can be run
directly in any computer. To test it, you can use the API POST TEST file (also 
in Python) to make a request to this API with any parameter you would like.