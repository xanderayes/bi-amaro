-- creates a temporary table with the first day of every month and its respective border dates between 5 and 2 months ago
with base_dates as (
  select distinct
    date_trunc('month', o.order_date)
      as current_month,
    dateadd(month, -2, date_trunc('month', o.order_date))
      as two_months_ago,
    dateadd(month, -5, date_trunc('month', o.order_date))
      as five_months_ago
  from orders o),

-- creates a temporary table based on the Orders table and includes a field with the first order date for the current user
-- I'm applying a filter to only bring orders that were actually paid
orders_with_first_date as (
  select
    *,
    first_value(o.order_date) over (partition by o.user_id order by o.order_date asc
      rows between unbounded preceding and unbounded following) as first_order_date
  from orders o
  where o.order_status not in ('CANCELLED', 'AWAITING PAYMENT CONFIRMATION')
  )


-- unites both temporary tables using the border dates as keys
-- the numerator is the number of clients that are present in both time ranges (5 and 2 months ago / 2 months ago and the current date)
-- the denominator is the number of clients that are present in the first time range (5 and 2 months ago) and not necessarily purchased
-- again on the last 2 months
select distinct
  bd.current_month,
  count(distinct owfd2.user_id) * 1.0 / nullif(count(DISTINCT owfd.user_id), 0)
    as retention_rate
from base_dates bd
  left join orders_with_first_date owfd
    on owfd.first_order_date >= bd.five_months_ago
      and owfd.first_order_date < bd.two_months_ago
  left join orders_with_first_date owfd2
    on owfd2.first_order_date >= bd.five_months_ago
      and owfd2.first_order_date < bd.two_months_ago
      and owfd2.order_date >= bd.two_months_ago
      and owfd2.order_date < bd.current_month
group by 1;

